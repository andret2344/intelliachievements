package eu.andret.intelliachievements.achievement.helper;

import com.intellij.execution.filters.ConsoleFilterProvider;
import com.intellij.execution.filters.Filter;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import eu.andret.intelliachievements.AchievementService;
import eu.andret.intelliachievements.achievement.type.ConsoleAchievement;
import org.jetbrains.annotations.NotNull;

public class ConsoleAchievementHelper implements ConsoleFilterProvider {
	@Override
	public Filter @NotNull [] getDefaultFilters(@NotNull final Project project) {
		return ApplicationManager.getApplication()
				.getService(AchievementService.class)
				.getByClass(ConsoleAchievement.class)
				.stream()
				.map(ConsoleAchievement.class::cast)
				.toArray(Filter[]::new);
	}
}
