package eu.andret.intelliachievements.achievement.type;

import eu.andret.intelliachievements.IntelliAchievements;

public abstract class MenuAchievement extends AbstractAchievement {
	protected MenuAchievement(final IntelliAchievements.AchievementsState state, final int... states) {
		super(state, states);
	}
}
