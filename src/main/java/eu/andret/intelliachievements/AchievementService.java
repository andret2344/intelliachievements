package eu.andret.intelliachievements;

import com.intellij.openapi.components.Service;
import eu.andret.intelliachievements.achievement.type.AbstractAchievement;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@Service
public final class AchievementService {
	private final Set<AbstractAchievement> achievements = new TreeSet<>();

	public void registerAchievement(final AbstractAchievement achievement) {
		achievements.add(achievement);
	}

	public <E extends AbstractAchievement> List<E> getByClass(@NotNull final Class<E> clazz) {
		return achievements.stream()
				.filter(a -> a.getClass().isAssignableFrom(clazz) || clazz.isAssignableFrom(a.getClass()))
				.map(clazz::cast)
				.collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
	}

	@NotNull
	public List<AbstractAchievement> getAllAchievements() {
		return new ArrayList<>(achievements);
	}
}
