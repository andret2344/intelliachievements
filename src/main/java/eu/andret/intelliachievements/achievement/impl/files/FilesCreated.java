package eu.andret.intelliachievements.achievement.impl.files;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectFileIndex;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.newvfs.events.VFileCreateEvent;
import com.intellij.openapi.vfs.newvfs.events.VFileEvent;
import eu.andret.intelliachievements.IntelliAchievements;
import eu.andret.intelliachievements.achievement.type.FileSystemAchievement;
import org.jetbrains.annotations.NotNull;

public class FilesCreated extends FileSystemAchievement {
	public FilesCreated(final IntelliAchievements.AchievementsState state, final int... states) {
		super(state, states);
	}

	@Override
	public String getName() {
		return "Hello new world!";
	}

	@Override
	public String getToolTipText() {
		return "Create " + getMatchingState() + " files.";
	}

	@Override
	public boolean isHidden() {
		return false;
	}

	@Override
	public int getCurrentValue() {
		return state.getFilesCreated();
	}

	@Override
	public void fileChangedAfter(@NotNull final VFileEvent event, @NotNull final Project project) {
		final ProjectFileIndex fileIndex = ProjectRootManager.getInstance(project).getFileIndex();
		if (event instanceof VFileCreateEvent && event.getFile() != null && fileIndex.isInContent(event.getFile())) {
			state.setFilesCreated(state.getFilesCreated() + 1);
			updated();
		}
	}
}
