package eu.andret.intelliachievements;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.components.PersistentStateComponent;
import com.intellij.openapi.components.Service;
import com.intellij.openapi.components.State;
import com.intellij.openapi.components.Storage;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.intellij.openapi.vfs.newvfs.BulkFileListener;
import eu.andret.intelliachievements.achievement.type.AbstractAchievement;
import eu.andret.intelliachievements.achievement.impl.easteregg.EasterEggFound;
import eu.andret.intelliachievements.achievement.impl.files.FilesCreated;
import eu.andret.intelliachievements.achievement.impl.files.FilesDeleted;
import eu.andret.intelliachievements.achievement.impl.keyboard.HelloWorld;
import eu.andret.intelliachievements.achievement.impl.keyboard.KeysTyped;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

@Service
@State(name = "achievements", storages = {
		@Storage("achievements.xml")
})
public final class IntelliAchievements implements PersistentStateComponent<IntelliAchievements.AchievementsState> {
	private AchievementsState achievementsState = new AchievementsState();

	public IntelliAchievements() {
		final AbstractAchievement[] typical = {
				new HelloWorld(achievementsState, 1, 10, 100),
				new KeysTyped(achievementsState, 100, 1_000, 1_000_000, 1_000_000_000, Integer.MAX_VALUE),
				new FilesCreated(achievementsState, 1, 10, 100, 1_000, 10_000),
				new FilesDeleted(achievementsState, 1, 10, 100, 1_000, 10_000),
				new EasterEggFound(achievementsState, 1)
		};

		Arrays.stream(typical).forEach(achievement -> {
			achievement.addOnStateUpdateListener((a, current) -> {
				if (Arrays.stream(a.getStates()).anyMatch(s -> s == current)) {
					Notifications.Bus.notify(new Notification("Achievements", a.getName(), a.getToolTipText(), NotificationType.INFORMATION));
				}
			});
			if (BulkFileListener.class.isAssignableFrom(achievement.getClass())) {
				ApplicationManager.getApplication().getMessageBus().connect().subscribe(VirtualFileManager.VFS_CHANGES, (BulkFileListener) achievement);
			}
			final AchievementService achievementService =
					ApplicationManager.getApplication().getService(AchievementService.class);
			achievementService.registerAchievement(achievement);
		});
	}

	@Override
	public AchievementsState getState() {
		return achievementsState;
	}

	@Override
	public void loadState(@NotNull final AchievementsState state) {
		achievementsState = state;
	}

	public static class AchievementsState {
		private int helloWorlds;
		private int keysTyped;
		private int filesCreated;
		private int filesDeleted;
		private int easterEggFound;

		public int getHelloWorlds() {
			return helloWorlds;
		}

		public void setHelloWorlds(int helloWorlds) {
			this.helloWorlds = helloWorlds;
		}

		public int getKeysTyped() {
			return keysTyped;
		}

		public void setKeysTyped(int keysTyped) {
			this.keysTyped = keysTyped;
		}

		public int getFilesCreated() {
			return filesCreated;
		}

		public void setFilesCreated(int filesCreated) {
			this.filesCreated = filesCreated;
		}

		public int getFilesDeleted() {
			return filesDeleted;
		}

		public void setFilesDeleted(int filesDeleted) {
			this.filesDeleted = filesDeleted;
		}

		public int getEasterEggFound() {
			return easterEggFound;
		}

		public void setEasterEggFound(int easterEggFound) {
			this.easterEggFound = easterEggFound;
		}
	}
}
