package eu.andret.intelliachievements.config;

import com.intellij.openapi.options.Configurable;
import eu.andret.intelliachievements.PluginSettingsForm;

import javax.swing.JComponent;

public class PluginConfigurable implements Configurable {
	private final PluginSettingsForm settingsForm = new PluginSettingsForm();

	@Override
	public String getDisplayName() {
		return "IntelliJ Achievements";
	}

	@Override
	public String getHelpTopic() {
		return "Displays the Achievements available to get";
	}

	@Override
	public JComponent createComponent() {
		return settingsForm.getRootComponent();
	}

	@Override
	public boolean isModified() {
		return false;
	}

	@Override
	public void apply() {
		// does nothing
	}

	@Override
	public void reset() {
		// does nothing
	}

	@Override
	public void disposeUIResources() {
		// does nothing
	}
}
