package eu.andret.intelliachievements.achievement.impl.easteregg;

import eu.andret.intelliachievements.IntelliAchievements;
import eu.andret.intelliachievements.achievement.type.MenuAchievement;

public class EasterEggFound extends MenuAchievement {
	public EasterEggFound(final IntelliAchievements.AchievementsState state, final int... states) {
		super(state, states);
	}

	@Override
	public String getName() {
		return "Is it an Easter time?";
	}

	@Override
	public String getToolTipText() {
		return "Find the Easter egg.";
	}

	@Override
	public boolean isHidden() {
		return true;
	}

	@Override
	public int getCurrentValue() {
		return state.getEasterEggFound();
	}
}
