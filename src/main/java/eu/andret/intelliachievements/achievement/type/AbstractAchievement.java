package eu.andret.intelliachievements.achievement.type;

import eu.andret.intelliachievements.IntelliAchievements;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public abstract class AbstractAchievement implements Comparable<AbstractAchievement> {
	private final int[] states;
	private final List<OnStateUpdateListener> onStateUpdateListeners = new ArrayList<>();
	protected final IntelliAchievements.AchievementsState state;

	public interface OnStateUpdateListener {
		void stateUpdated(AbstractAchievement a, int current);
	}

	AbstractAchievement(final IntelliAchievements.AchievementsState state, final int... states) {
		this.state = state;
		this.states = states;
	}

	public abstract String getName();

	public abstract String getToolTipText();

	public abstract boolean isHidden();

	public abstract int getCurrentValue();

	public IntelliAchievements.AchievementsState getState() {
		return state;
	}

	public final void updated() {
		for (final OnStateUpdateListener listener : onStateUpdateListeners) {
			listener.stateUpdated(this, getCurrentValue());
		}
	}

	public final int getMatchingState() {
		return Arrays.stream(getStates())
				.filter(i -> i > getCurrentValue())
				.findFirst()
				.orElse(states[states.length - 1]);
	}

	public final int[] getStates() {
		return states;
	}

	public final void addOnStateUpdateListener(final OnStateUpdateListener listener) {
		onStateUpdateListeners.add(listener);
	}

	@Override
	public String toString() {
		return "AbstractAchievement{" +
				"states=" + Arrays.toString(states) +
				", onStateUpdateListeners=" + onStateUpdateListeners +
				", state=" + state +
				'}';
	}

	@Override
	public final boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final AbstractAchievement that = (AbstractAchievement) o;
		return Arrays.equals(getStates(), that.getStates()) &&
				Objects.equals(getCurrentValue(), that.getCurrentValue()) &&
				Objects.equals(getName(), that.getName());
	}

	@Override
	public final int hashCode() {
		return Objects.hash(Arrays.hashCode(getStates()), getCurrentValue(), getName());
	}

	@Override
	public int compareTo(@NotNull final AbstractAchievement achievement) {
		if (isHidden() == achievement.isHidden()) {
			return getName().compareTo(achievement.getName());
		}
		return (isHidden() ? 1 : 0) - (achievement.isHidden() ? 1 : 0);
	}
}
