package eu.andret.intelliachievements.achievement.impl.keyboard;

import eu.andret.intelliachievements.IntelliAchievements;
import eu.andret.intelliachievements.achievement.type.EditorAchievement;

public class KeysTyped extends EditorAchievement {
	public KeysTyped(final IntelliAchievements.AchievementsState state, final int... states) {
		super(state, states);
	}

	@Override
	public void charTyped(final char c) {
		if (c < 'z' && c > 'a' || c < '9' && c > '0' || c < 'Z' && c > 'A') {
			state.setKeysTyped(state.getKeysTyped() + 1);
			updated();
		}
	}

	@Override
	public String getName() {
		return "Keyboard master";
	}

	@Override
	public String getToolTipText() {
		return "Type " + getMatchingState() + " symbols";
	}

	@Override
	public boolean isHidden() {
		return false;
	}

	@Override
	public int getCurrentValue() {
		return state.getKeysTyped();
	}
}
