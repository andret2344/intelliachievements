package eu.andret.intelliachievements.achievement.impl.keyboard;

import com.intellij.execution.filters.Filter;
import eu.andret.intelliachievements.IntelliAchievements;
import eu.andret.intelliachievements.achievement.type.ConsoleAchievement;
import org.jetbrains.annotations.NotNull;

public class HelloWorld extends ConsoleAchievement {
	public HelloWorld(final IntelliAchievements.AchievementsState state, final int... states) {
		super(state, states);
	}

	@Override
	public String getName() {
		return "Greet the world!";
	}

	@Override
	public String getToolTipText() {
		return "Type traditional \"Hello World\".";
	}

	@Override
	public boolean isHidden() {
		return false;
	}

	@Override
	public int getCurrentValue() {
		return state.getHelloWorlds();
	}

	@Override
	public Filter.Result applyFilter(@NotNull final String line, final int entireLength) {
		if (line.toLowerCase().contains("Hello World".toLowerCase())) {
			state.setHelloWorlds(state.getHelloWorlds() + 1);
			updated();
		}
		return null;
	}
}
