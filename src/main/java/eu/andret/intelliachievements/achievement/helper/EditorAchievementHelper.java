package eu.andret.intelliachievements.achievement.helper;

import com.intellij.codeInsight.editorActions.TypedHandlerDelegate;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFile;
import eu.andret.intelliachievements.AchievementService;
import eu.andret.intelliachievements.achievement.type.EditorAchievement;
import org.jetbrains.annotations.NotNull;

public class EditorAchievementHelper extends TypedHandlerDelegate {
	@NotNull
	@Override
	public Result charTyped(final char c, @NotNull final Project project, @NotNull final Editor editor, @NotNull final PsiFile file) {
		ApplicationManager.getApplication()
				.getService(AchievementService.class)
				.getByClass(EditorAchievement.class)
				.forEach(achievement -> achievement.charTyped(c));
		return Result.CONTINUE;
	}
}
