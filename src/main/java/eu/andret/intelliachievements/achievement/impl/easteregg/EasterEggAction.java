package eu.andret.intelliachievements.achievement.impl.easteregg;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.ui.Messages;
import eu.andret.intelliachievements.AchievementService;
import org.jetbrains.annotations.NotNull;

class EasterEggAction extends AnAction {
	@Override
	public void actionPerformed(@NotNull final AnActionEvent anActionEvent) {
		Messages.showDialog("You have found the Easter Egg!", "Easter Egg", new String[]{"OK"}, 1, null);
		anActionEvent.getPresentation().setEnabledAndVisible(true);
		final EasterEggFound easterEggFound = ApplicationManager.getApplication()
				.getService(AchievementService.class)
				.getByClass(EasterEggFound.class)
				.get(0);
		easterEggFound.getState().setEasterEggFound(1);
		easterEggFound.updated();
	}
}
