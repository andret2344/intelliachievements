plugins {
    id("org.jetbrains.intellij") version "1.17.2"
}

repositories {
    mavenCentral()
}

intellij {
    version.set("2022.2")
    plugins.set(listOf("com.intellij.java"))
}

val jetbrainsAnnotationsVersion = "24.1.0"

dependencies {
    implementation("org.jetbrains:annotations:${jetbrainsAnnotationsVersion}")
}

tasks {
    compileJava {
        sourceCompatibility = "17"
        targetCompatibility = "17"
        options.compilerArgs.addAll(listOf("-parameters", "-g", "-Xlint:deprecation", "-Xlint:unchecked"))
    }

    withType<org.jetbrains.intellij.tasks.BuildSearchableOptionsTask>().configureEach {
        enabled = false
    }

    withType<org.jetbrains.intellij.tasks.PatchPluginXmlTask>().configureEach {
        version.set(project.version.toString())
        sinceBuild.set("222")
    }
}
